﻿using MyQueue.Domain.Implementations;
using MyQueue.Helpers;
using MyQueueService.MyQueue;
using System;
using System.Linq;

namespace MyQueue
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Здравствуйте! Введите данные:\n");
			try
			{
				//Считываем данные в массив
				string[] data = Console.ReadLine().Split(' ').ToArray();

				//Берем первое число, которое укажет сколько показать элементов
				var countOfNumbers = 0;
				var result = Int32.TryParse(data[0], out countOfNumbers);
				if (result == false)
				{
					throw new Exception("Число элементов указано некорректно!\n");
				}

				//Если число отрицательное или больше количества объектов в массиве, то покажем весь массив
				if (countOfNumbers > data.Length - 1 || countOfNumbers < 1)
				{
					countOfNumbers = data.Length - 1;
				}

				//Создаем объект MyQueue с помощью хелпера
				var helper = new PersonHelper();
				var queue = (MyQueue<Person>)null;
				helper.NewQueue(data, out queue);

				//Если очередь не пуста, то покажем объекты
				if (queue.Queue != null)
				{
					for (int i = 0; i < countOfNumbers; i++)
					{
						Console.WriteLine(queue.Queue[i].Phone);
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			Console.ReadKey();
		}
	}
}
