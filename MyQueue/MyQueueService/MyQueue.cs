﻿using MyQueue.Domain.Interfaces;
using System;

namespace MyQueueService.MyQueue
{
	/// <summary>
	/// Обощеный класс MyQueue
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class MyQueue<T> where T : class, IEntity
	{
		public T[] Queue { get; set; }

		public MyQueue(params T[] data)
		{
			if (data != null)
			{
				Queue = new T[data.Length];
				for (int i = 0; i < data.Length; i++)
				{
					Queue[i] = data[i];
				}
			}
			else
			{
				Console.WriteLine("Невалидные данные!\n");
				return;
			}
		}
		public T Dequeue()
		{
			if (Queue.Length > 0)
			{
				T firstElement = Queue[0];
				var NewQueue = new T[Queue.Length - 1];
				for (int i = 0; i < Queue.Length - 1; i++)
				{
					NewQueue[i] = Queue[i + 1];
				}
				Queue = NewQueue;
				return firstElement;
			}
			else
			{
				throw new Exception("Очередь пуста!");
			}
		}

		public void Enqueue(T data)
		{
			var NewQueue = new T[Queue.Length + 1];
			for (int i = 0; i < Queue.Length + 1; i++)
			{
				if (i < Queue.Length)
				{
					NewQueue[i] = Queue[i];
				}
				else
				{
					NewQueue[i] = data;
				}
				Queue = NewQueue;
			}
		}
		public bool IsEmpty()
		{
			if (Queue.Length > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public int Size()
		{
			var size = Queue.Length;
			return size;
		}

		public void SortByPhoneNumber() { }
	}
}
