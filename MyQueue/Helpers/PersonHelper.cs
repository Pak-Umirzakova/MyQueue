﻿using MyQueue.Domain.Implementations;
using MyQueueService.MyQueue;
using System;
using System.Text.RegularExpressions;

namespace MyQueue.Helpers
{
	/// <summary>
	/// Хэлпер для создания MyQueue для объекта Person
	/// </summary>
	public class PersonHelper
	{
		/// <summary>
		/// Создание нового объекта Myqueue
		/// </summary>
		/// <param name="numbers"></param>
		/// <param name="queue"></param>
		/// <returns></returns>
		public MyQueue<Person> NewQueue(string[] numbers, out MyQueue<Person> queue)
		{
			queue = null;
			try
			{
				queue = new MyQueue<Person>(NewPersonArray(numbers));
			}
			catch(Exception ex)
			{
				throw;
			}
			return queue;
		}

		/// <summary>
		/// Создание массива объектов Person
		/// </summary>
		/// <param name="numbers"></param>
		/// <returns></returns>
		private Person[] NewPersonArray(string[] numbers)
		{
			var people = (Person[])null;
			try
			{
				people = new Person[numbers.Length - 1];
				for (int i = 0; i < numbers.Length-1; i++)
				{
					var person = (Person)null;
					var isNumber = IsNumber(numbers[i+1]);
					if (isNumber == true)
					{
						person = new Person(numbers[i + 1]) { };
						people[i] = person;
					}
					else
					{
						Console.WriteLine("Номер номер " + (i + 1) + " введен некорректно! Транзакция остановлена!");
						return null;
					}
				}
			}
			catch (Exception ex)
			{
				throw;
			}
			return people;
		}

		/// <summary>
		/// Валидация номеров телефона
		/// </summary>
		/// <param name="number"></param>
		/// <returns></returns>
		private bool IsNumber(string number)
		{
			try
			{
				Regex regex = new Regex(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$");
				return regex.IsMatch(number);
			}
			catch (Exception ex)
			{
				throw;
			}
		}

	}
}
