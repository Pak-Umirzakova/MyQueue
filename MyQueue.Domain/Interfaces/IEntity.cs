﻿using System;

namespace MyQueue.Domain.Interfaces
{
	public interface IEntity
	{
		Guid Id{ get; set; }
	}
}
