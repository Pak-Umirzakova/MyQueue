﻿namespace MyQueue.Domain.Interfaces
{
	public interface IPerson: IEntity
	{
		string Phone { get; set; }
	}
}
