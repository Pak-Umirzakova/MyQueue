﻿using MyQueue.Domain.Interfaces;
using System;

namespace MyQueue.Domain.Abstract
{
	public abstract class Entity : IEntity
	{
		public Guid Id { get; set; }

		public Entity()
		{
			Id = Guid.NewGuid();
		}
	}
}
