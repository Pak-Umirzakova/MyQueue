﻿using MyQueue.Domain.Abstract;
using MyQueue.Domain.Interfaces;
using System;

namespace MyQueue.Domain.Implementations
{
	public class Person : Entity, IPerson
	{
		public string Phone { get; set; }

		public Person(string phone) : base()
		{
			if (phone.Length > 0)
				Phone = phone;
			else
				throw new ArgumentNullException("Phone");
		}
	}
}
